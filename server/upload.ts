import express from 'express'
import multer from 'multer'

let upload = multer({
  storage: multer.diskStorage({
    destination: './upload',
    filename: (req, file, cb) => {
      let filename = Math.random().toString() + '.image'
      cb(null, filename)
    },
  }),
})

let router = express.Router()

export default router

router.get('/upload', (req, res) => {
  res.json('upload data')
})

// router.post('/upload', upload.array('images', 4), (req, res) => {
//   console.log('body:', req.body)
//   console.log('file:', req.file)
//   console.log('files:', req.files)
//   res.end('todo')
// })

router.post('/upload', (req, res) => {
  // upload.single('images')(req, res, (error: any) => {
  //   console.log('file:', req.file)
  //   res.end('ok')
  // })
  upload.array('images', 4)(req, res, (error: any) => {
    if (error && error.code === 'LIMIT_UNEXPECTED_FILE') {
      res.status(400).json({ error: 'at most 4 images' })
      return
    }
    if (!req.files) {
      res.status(400).json({ error: 'missing images' })
      return
    }
    if (req.files.length < 1) {
      res.status(400).json({ error: 'require at least 1 images' })
      return
    }
    console.log('error:', error)
    console.log('body:', req.body)
    console.log('file:', req.file)
    console.log('files:', req.files)
    res.json({ data: 'todo' })
  })
})
