import { selectImage, filesToBase64Strings } from '@beenotung/tslib/file'
import { toImage,compressImageToBase64, dataURItoBlob } from '@beenotung/tslib/image'
import React, { useState } from 'react'
import './App.css'

export function AppWithoutFile() {
  const [urlList, setUrlList] = useState<string[]>([])
  async function select() {
    let files = await selectImage({ multiple: true })
    let urlList = await filesToBase64Strings(files)
    let i = 0
    for (let url of urlList) {
      let image = await toImage(url)
      url = compressImageToBase64({image,maximumLength:100*1000})
      urlList[i] = url
    }
    setUrlList(urlList)
  }
  async function upload() {
    let body = new FormData()
    for (let url of urlList) {
      let blob = await dataURItoBlob(url)
      let file = new File([blob], 'image', { lastModified: Date.now() })
      body.append('images', file)
      console.log(blob)
    }
    fetch('http://127.0.0.1:8100/upload', { method: 'POST', body })
  }
  return (
    <div className="App">
      <button onClick={select}>select</button>
      <div>
        length:
        {urlList.length}
      </div>
      <button onClick={upload}>upload</button>
      <div>
        {urlList.map(url => (
          <img src={url} title={url.length.toLocaleString()}></img>
        ))}
      </div>
    </div>
  )
}

export function AppWithFile() {
  const [files, setFiles] = useState<File[]>([])
  async function select() {
    let files = await selectImage({ multiple: true })
    setFiles(files)
  }
  function upload() {
    let body = new FormData()
    files.forEach(file => {
      body.append('images', file)
    })
    fetch('http://127.0.0.1:8100/upload', { method: 'POST', body })
  }
  return (
    <div className="App">
      <button onClick={select}>select</button>
      <div>
        files:
        {files.length}
      </div>
      <button onClick={upload}>upload</button>
    </div>
  )
}

export default AppWithoutFile
